package mappe.del1.hospital;

import java.util.ArrayList;

public class Hospital {
    private final String HOSPITAL_NAME;
    private ArrayList<Department> departments;

    /**
     * Creates an instance of the object Hospital
     * @param hospitalName Name of the hospital
     * @throws IllegalArgumentException  If the hospital name is empty or null
     * an exception will be thrown
     */
    public Hospital(String hospitalName) throws IllegalArgumentException{
        if(hospitalName == null ||hospitalName.isEmpty()){
            throw new IllegalArgumentException("Hospital name can not be empty");
        }else{
            this.HOSPITAL_NAME = hospitalName;
        }
        this.departments = new ArrayList<>();
    }

    public String getHOSPITAL_NAME() {
        return HOSPITAL_NAME;
    }
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Method for adding a department to the departments list
     * @param department takes a department object as parameter
     * @throws IllegalArgumentException if the employee that should be added is already
     * added an exception will be thrown
     */
    public void addDepartment(Department department) throws IllegalArgumentException{ {
        if (!departments.contains(department)){
            departments.add(department);
        }else{
            throw new IllegalArgumentException("A department with the given name is already registered");
            }
        }
    }

    @Override
    public String toString() {
        return "Hospital: " + HOSPITAL_NAME  +
                "\n---------------------\nDepartments:\n" + departments;
    }
}
