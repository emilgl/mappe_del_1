package mappe.del1.hospital;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Creates an abstract instance of the object Person As long as one
     * of the parameters is not empty or null
     * @param firstName            firstname
     * @param lastName             lastname
     * @param socialSecurityNumber social security number
     * @throws IllegalArgumentException if all the parameters is null or empty an exception
     * will be thrown
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) throws IllegalArgumentException {
        if ((firstName == null || firstName.isEmpty()) && (lastName == null || lastName.isEmpty()) &&
                (socialSecurityNumber == null || socialSecurityNumber.isEmpty())) {
            throw new IllegalArgumentException("Firstname, lastname and social security number can not all be empty");
        } else {
            this.firstName = firstName;
            this.lastName = lastName;
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFullName() {
        return firstName + lastName;
    }

    @Override
    public String toString() {
        return "\nName: " + firstName + " " + lastName +
                "\nSocial security number: " + socialSecurityNumber + "\n";
    }


    /**
     * Equals method to compare the parameter with already registered objects in the add
     * and remove methods
     *
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!getFirstName().equals(person.getFirstName())) return false;
        if (!getLastName().equals(person.getLastName())) return false;
        return getSocialSecurityNumber().equals(person.getSocialSecurityNumber());
    }

}
