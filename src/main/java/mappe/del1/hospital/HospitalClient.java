package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

public class HospitalClient {

    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("Ahus");
        HospitalTestData.fillRegisterWithTestData(hospital);
        for (Department d : hospital.getDepartments()) {
            d.remove(new Employee("Odd Even", "Primtallet", ""));
            System.out.println(hospital.toString());
            break;
        }

        for(Department d: hospital.getDepartments()){
            try{
                d.remove(new Patient("Ola","Normann", "1002" ));
            }catch(RemoveException re){
                System.out.println(re.getMessage() + "in the following department: "+d.getDepartmentName() +"\n");
            }

        }

    }
}
