package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Creates an instance of the object Department
     * @param departmentName Name of the department
     * @throws IllegalArgumentException   If the department name is empty or null
     * an exception will be thrown
     */
    public Department(String departmentName) throws IllegalArgumentException{
        if(departmentName == null || departmentName.isEmpty()){
            throw new IllegalArgumentException("Department name can not be empty");
        }else{
            this.departmentName = departmentName;
        }
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    public String getDepartmentName() {
        return this.departmentName;
    }
    public ArrayList<Employee> getEmployees(){
        return this.employees;
    }
    public ArrayList<Patient> getPatients() {
        return this.patients;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Method for adding an employee to the list of employees
     * @param employee takes an employee object as parameter
     * @throws IllegalArgumentException if the employee that should be added is already
     * added an exception will be thrown
     */
    public void addEmployee(Employee employee) throws IllegalArgumentException{
        if(!employees.contains(employee)){
            this.employees.add(employee);
        }else{
            throw new IllegalArgumentException("This employee is already registered");
        }
    }
    /**
     * Method for adding a patient to the list of patients
     * @param patient takes a patient object as parameter
     * @throws IllegalArgumentException if the patient that should be added is already
     * added an exception will be thrown
     */
    public void addPatient(Patient patient) throws IllegalArgumentException{
        if (!patients.contains(patient)) {
            this.patients.add(patient);
        } else {
            throw new IllegalArgumentException("This patient is already registered");
        }
    }

    /**
     * Method for removing a person either a patient or employee
     * @param person takes a person object as parameter
     * @throws RemoveException if the person supposed to be remove is not a
     * part of either the patients or employees lists the removeException
     * method in the Remove exception class will be called upon and an exception of type
     * RemoveException will be thrown
     */
    public void remove(Person person) throws RemoveException {
        if (patients.contains(person)){
            patients.remove(person);
        }else if (employees.contains(person)){
            employees.remove(person);
        }else {
            throw new RemoveException(person.toString());
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(getDepartmentName(), that.getDepartmentName()) && Objects.equals(getEmployees(), that.getEmployees()) && Objects.equals(getPatients(), that.getPatients());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName(), getEmployees(), getPatients());
    }

    @Override
    public String toString() {
        return "\nDepartment: " + departmentName +
                "\n---------------------\nEmployees:\n" + employees +
                "\n---------------------\nPatients:\n" + patients;
    }
}
