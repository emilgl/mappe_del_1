package mappe.del1.hospital;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
public class PositiveTestRemove {

    @Test
    public void testRemove(){
        Department department = new Department("Akutten");
        Patient patient1 = new Patient("Ola", "Normann", "1001");
        Patient patient2 = new Patient("Kari", "Normann", "1002");
        Employee employee1 = new Employee("Roger", "Ruud", "1003");
        Employee employee2 = new Employee("Ansatt", "Ansattsen", "1004");
        department.addPatient(patient1);
        department.addPatient(patient2);
        department.addEmployee(employee1);
        department.addEmployee(employee2);

        assertDoesNotThrow(()-> department.remove(patient1));
        assertDoesNotThrow(()->department.remove(employee1));
        assertEquals(department.getPatients().size(),1);
        assertEquals(department.getEmployees().size(),1 );
        assertNotEquals(department.getPatients().get(0), patient1);
        assertNotEquals(department.getEmployees().get(0), employee1);
    }
}
