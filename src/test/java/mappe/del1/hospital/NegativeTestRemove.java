package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class NegativeTestRemove {

    @Test
    public void testRemove(){
        Department department = new Department("Akutten");
        Patient patient1 = new Patient("Kari", "Normann", "1002");

        assertThrows(RemoveException.class, ()->department.remove(patient1));
    }
}
